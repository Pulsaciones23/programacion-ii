﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Avion.Clases
{
    public class Personaje:Entidad
    {
        public List<Bala> Balas { get; set; }
        public Personaje(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color) :base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color
            )
        {
                this._velocidad_X = 0;
                this._velocidad_Y = 0;
                this._posicion=new Vector2(300,300);
                this._pantalla_alto=pantalla_alto;
                this._pantalla_largo=pantalla_largo;        
                this._spriteBatch=sb;
            this.Balas = new List<Bala>();
            this.Init(cm, this._spriteBatch);

    }

        private void Init(ContentManager cm, SpriteBatch sb)
        {
            this._listaTexturas.Add(cm.Load<Texture2D>("avion_Piloto_IDLE"));
            this._listaTexturas.Add(cm.Load<Texture2D>("avion_Piloto_IZ_2"));
            this._listaTexturas.Add(cm.Load<Texture2D>("avion_Piloto_de_2"));
            this._posicion = new Vector2(this._pantalla_largo / 2, this._pantalla_alto - 100);
            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;
            this.Velocidad = 3;
            this.Imagen = this._listaTexturas[0];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            //base.Mover(velecidad_x, velocidad_y);
            float v = (velocidad_x * this.Velocidad);
            if (((this._posicion.X) + v) >= 0 && (((this._posicion.X ) + this.Largo) + v) <= this._pantalla_largo)
            {
                this._posicion.X += v;
            }
            this._posicion.Y += (velocidad_y) * this.Velocidad;
            int img = 0;
            
            if (velocidad_x > 0)
            {img = 2;}
            else {
                if (velocidad_x < 0)
                {img = 1;}
                else {
                    img = 0;}
            }
            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;
            this.Imagen = this._listaTexturas[img];
        }

        public override void Reinicializar()
        {
            throw new NotImplementedException();
        }
    }
}
