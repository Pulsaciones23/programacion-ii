﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Avion.Clases
{
    public class Bala:Entidad
    {
        public Bala(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color, float personajeX, float personajeY) : base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color)
        {

            this.Init(cm, sb, personajeX, personajeY);

        }
        private void Init(ContentManager cm, SpriteBatch sb, float personajeX, float personajeY)
        {
            if (Controlador.numBala == 0)
            {
                this._listaTexturas.Add(cm.Load<Texture2D>("bala0"));
            }
            if (Controlador.numBala == 1)
            {
                this._listaTexturas.Add(cm.Load<Texture2D>("bala1"));
            }
            if (Controlador.numBala == 2)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala2"));
            }
            if (Controlador.numBala == 3)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala3"));
            }
            if (Controlador.numBala == 4)
            {
             this._listaTexturas.Add(cm.Load<Texture2D>("bala4"));
            }
            if (Controlador.numBala == 5)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala5"));
            }
            if (Controlador.numBala == 6)
            {
             this._listaTexturas.Add(cm.Load<Texture2D>("bala6"));
            }
            if (Controlador.numBala == 7)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala7"));
            }
            if (Controlador.numBala == 8)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala8"));
            }
            if (Controlador.numBala == 9)
            {
            this._listaTexturas.Add(cm.Load<Texture2D>("bala9"));
            }

            this._posicion.X = personajeX;
            this._posicion.Y = personajeY;

            this.Velocidad = 1.4f;

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;

            this.Imagen = this._listaTexturas[0];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            this._posicion.Y -=  this.Velocidad;

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;

        }

        public override void Reinicializar()
        {
            throw new NotImplementedException();
        }
    }
}
