﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Avion.Clases
{
    public class Controlador
    {
        public static int numBala;
        private int _pantalla_Alto;
        private int _pantalla_Largo;
        private SpriteBatch _spriteBatch;
        private ContentManager _context;
        private float _velocidad_X=0;
        private float _velocidad_Y=0;
        private int _cantidadEnemigos = 10;
        private int _cantidadBalas;
        private int _ciclo = 0;
        private int damage = 0;
        private bool PersmioParaDisparar = true;
        private int timer = 0;
        public List<Entidad> Entidades { get; set; }
        private Personaje _personaje;

        public Controlador() {
           
        }
        public Controlador(int pantlla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb
        )
        {
            this._pantalla_Alto=pantlla_alto;
            this._pantalla_Largo=pantalla_largo;
            this._spriteBatch=sb;
            this._context=cm;

            this._cantidadBalas=50;
            this._ciclo=0;

            this.Entidades = new List<Entidad>();
            this._personaje = new Personaje(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White);
            this.AgregarEntidad();
    }
        public void BorrarEntidad() { }
        public void ACtualizar() {
            this.LeerTeclas();
            this._ciclo += 1;
            this._personaje.Mover(this._velocidad_X, _velocidad_Y);


            timer--;
            if (timer <= 0)
            {
                PersmioParaDisparar = true;
            }


            foreach(Entidad e in this.Entidades)
            {
                if (e.EstadoEntidad != Entidad.Estado.COLISION)
                {
                    e.Mover(0, 0.5f);
                    if (e.Y > this._pantalla_Largo)
                    {
                        e.Reinicializar();
                    }
                }
            }
            foreach (Bala b in this._personaje.Balas)
            {
                if (b.EstadoEntidad != Entidad.Estado.COLISION)
                {
                    b.Mover(0, 1f);
                }
            }

            //#region "Colision con Personaje"
            //    Rectangle colliderPersonaje = new Rectangle(this._personaje.X +10, this._personaje.Y+10, this._personaje.Largo-10, this._personaje.Alto -10);

            //    foreach (Entidad e in this.Entidades)
            //    {
            //    if (e.EstadoEntidad != Entidad.Estado.COLISION)
            //    {
            //        Rectangle colliderAvion = new Rectangle(e.X, e.Y, e.Largo, e.Alto);
            //        if (colliderPersonaje.Intersects(colliderAvion))
            //        {
            //            //pummm
            //            e.EstadoEntidad = Entidad.Estado.COLISION;
            //            // borrar el avion de la lista
            //        }
            //    }
            //    }
            //#endregion

            #region "Colision Bala con Avion"
            foreach (Bala b in this._personaje.Balas)
            {
                if (b.EstadoEntidad != Entidad.Estado.COLISION)
                {
                    Rectangle colliderBala = new Rectangle(b.X, b.Y, b.Largo, b.Alto);

                    foreach (Entidad e in this.Entidades)
                    {
                        if (e.EstadoEntidad != Entidad.Estado.COLISION)
                        {
                            Rectangle colliderAvion = new Rectangle(e.X, e.Y, e.Largo, e.Alto);
                            if (colliderBala.Intersects(colliderAvion))
                            {
                                if (e.ValorResultado == damage)
                                {
                                    e.EstadoEntidad = Entidad.Estado.COLISION;
                                    b.EstadoEntidad = Entidad.Estado.COLISION;
                                    PersmioParaDisparar = true;
                                }

                            }
                        }
                    }
                }
            }
            #endregion

            this._velocidad_X = 0;
            this._velocidad_Y = 0;
        }
        private void LeerTeclas() {

            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {

                this._velocidad_X = -1f;
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                {
                    this._velocidad_X = 1f;
                }
            }

            if (PersmioParaDisparar == true) { 
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad1) || Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                        numBala = 1;
                        damage = 1;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                        PersmioParaDisparar = false;

                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad2) || Keyboard.GetState().IsKeyDown(Keys.D2))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 2;
                        damage = 2;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad3) || Keyboard.GetState().IsKeyDown(Keys.D3))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 3;
                        damage = 3;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad4) || Keyboard.GetState().IsKeyDown(Keys.D4))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 4;
                        damage = 4;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad5) || Keyboard.GetState().IsKeyDown(Keys.D5))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 5;
                        damage = 5;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad6) || Keyboard.GetState().IsKeyDown(Keys.D6))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 6;
                        damage = 6;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad7) || Keyboard.GetState().IsKeyDown(Keys.D7))
            {

                    if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                    {
                    numBala = 7;
                    damage = 7;
                    timer = 200;
                    Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                    }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad8) || Keyboard.GetState().IsKeyDown(Keys.D8))
            {
                    if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                    {
                        numBala = 8;
                        damage = 8;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                     }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad9) || Keyboard.GetState().IsKeyDown(Keys.D9))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 9;
                        damage = 9;
                        timer = 200;
                        Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.NumPad0) || Keyboard.GetState().IsKeyDown(Keys.D0))
            {
                if (this._ciclo % 10 == 0 && this.PersmioParaDisparar)
                {
                    numBala = 0;
                    damage = 0;
                    timer = 200;
                    Bala b = new Bala(this._pantalla_Alto, this._pantalla_Largo, this._context, this._spriteBatch, Color.White, this._personaje.X + 9, this._personaje.Y);
                    this._personaje.Balas.Add(b);
                    PersmioParaDisparar = false;
                }
            }
            }
        }
        public void Dibujar() {

            this._personaje.Dibujar();
            foreach (Entidad e in this.Entidades)
            {
                e.Dibujar();
            }

            foreach (Bala b in this._personaje.Balas)
            {
                if (b.EstadoEntidad != Entidad.Estado.COLISION)
                {
                    b.Dibujar();
                }
            }

        }
        private void AgregarEntidad() {
        
            for (int i=0; i < this._cantidadEnemigos; i++)
            {
                this.Entidades.Add(new Avion(this._pantalla_Alto,this._pantalla_Largo,this._context,this._spriteBatch,Color.White));
            }
        }


    }
}
