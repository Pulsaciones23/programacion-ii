﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace Avion.Clases
{
    class Resultado : enemigo
    {



        public Resultado(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color) : base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color)
        {

            this.Init(cm, sb);

        }
        private void Init(ContentManager cm, SpriteBatch sb)
        {
            #region imagenes
            this._listaTexturas.Add(cm.Load<Texture2D>("0"));
            this._listaTexturas.Add(cm.Load<Texture2D>("1"));
            this._listaTexturas.Add(cm.Load<Texture2D>("2"));
            this._listaTexturas.Add(cm.Load<Texture2D>("3"));
            this._listaTexturas.Add(cm.Load<Texture2D>("4"));
            this._listaTexturas.Add(cm.Load<Texture2D>("5"));
            this._listaTexturas.Add(cm.Load<Texture2D>("6"));
            this._listaTexturas.Add(cm.Load<Texture2D>("7")); 
            this._listaTexturas.Add(cm.Load<Texture2D>("8")); 
            this._listaTexturas.Add(cm.Load<Texture2D>("9")); 


            #endregion

            var random = new Random();

            this._posicion.X = random.Next(100, 500);
            this._posicion.Y = -1 * (random.Next(10, 50));

            this.Velocidad = random.Next(1, 3);

            this.X = (int)this._posicion.X;
            //this.Y = (int)this._posicion.Y;


            int valor = random.Next(0, 9);
            this.Imagen = this._listaTexturas[valor];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
            this.ValorResultado = valor;



        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            this._posicion.Y += velocidad_y * this.Velocidad;

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;

        }
        public override void Dibujar()

        {
                base.Dibujar();
            

        }
        public override void Reinicializar()
        {
            var random = new Random();

            this._posicion.X = random.Next(100, 500);
            this._posicion.Y = -1 * (random.Next(10, 50));

            this.Velocidad = random.Next(1, 3);

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;



            int valor = random.Next(0, 9);
            this.Imagen = this._listaTexturas[valor];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
            this.ValorResultado = valor;
        }
    }
}