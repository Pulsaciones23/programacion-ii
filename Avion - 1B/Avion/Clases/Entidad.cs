﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Avion.Clases
{
    public abstract class Entidad
    {
        #region "Enum"
        public enum Estado
        {
            IDLE = 0,
            MOVIENDOSE = 1,
            SALTANDO = 2,
            COLISION = 3
        }
        public enum IMG_Direccion
        {

            IDLE = 0,
            IZQUIERDA = 1,
            DERECHA = 2,
            ARRIBA = 3,
            ABAJO = 4
        }

        public enum Tipo_Resultado
        {
            NO_INTERACTUADO = 0,
            CORRECTO = 1,
            INCORRECTO = 2,

        }
        #endregion




        #region "Variables"
        protected private float _velocidad_X = 0;
        protected private float _velocidad_Y = 0;
        protected private Vector2 _posicion;
        protected private int _pantalla_alto;
        protected private int _pantalla_largo;
        protected private List<Texture2D> _listaTexturas = new List<Texture2D>();
        protected private SpriteBatch _spriteBatch;
        protected private Texture2D _img_Colision;
        #endregion

        public int X { get; set; }

        public int ValorResultado { get; set; }

        public int Y { get; set; }
        public int Largo { get; set; }
        public int Alto { get; set; }
        public Texture2D Imagen { get; set; }
        public Estado EstadoEntidad { get; set; }
        public Tipo_Resultado TipoResultado { get; set; }
        public Color ColorEntidad { get; set; }
        public float Velocidad { get; set; }
        public Entidad() { }
        public Entidad(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color
            )
        {
            this.EstadoEntidad = Estado.IDLE;
            this._spriteBatch = sb;
            this._pantalla_alto = pantalla_alto;
            this._pantalla_largo = pantalla_largo;
            this._listaTexturas = new List<Texture2D>();
            this.ColorEntidad = color;
        }

        public abstract void Reinicializar();

        public virtual void Dibujar() {

           this._spriteBatch.Draw(this.Imagen, this._posicion, this.ColorEntidad);
        }
        public abstract void Mover(float velocidad_x, float velocidad_y);
            
        public virtual void Parar() {
            
        }
    }
}
