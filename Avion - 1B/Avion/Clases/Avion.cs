﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace Avion.Clases
{
    public class Avion:enemigo
    {
        public int ContadorVida { get; set; }
        public Avion(int pantalla_alto,
            int pantalla_largo,
            ContentManager cm,
            SpriteBatch sb,
            Color color) : base(pantalla_alto,
            pantalla_largo,
            cm,
            sb,
            color)
        {
            this.ContadorVida = 0;
            this.Init(cm,sb);

        }
        private void Init(ContentManager cm, SpriteBatch sb)
        {
            this._img_Colision= (cm.Load<Texture2D>("pum_nave_chica"));
            var random = new Random();

            this._posicion.X = random.Next(100, 500); 
            this._posicion.Y = -1 * (random.Next(10, 50)) ;

            this.Velocidad = random.Next(1, 3);

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;

            this.Imagen = this._listaTexturas[ValorResultado];
            this.Largo = this.Imagen.Width;
            this.Alto = this.Imagen.Height;
        }
        public override void Mover(float velocidad_x, float velocidad_y)
        {
            this._posicion.Y += velocidad_y * this.Velocidad;

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;

        }
        public override void Dibujar()
        {
            if (this.EstadoEntidad == Estado.COLISION)
            {
                this.Imagen = this._img_Colision;
                this.ContadorVida += 1;
            }

            if (this.ContadorVida <= 40)
            {
                base.Dibujar();
            }
        }
        public override void Reinicializar()
        {
            var random = new Random();

            this._posicion.X = random.Next(100, 500);
            this._posicion.Y = -1 * (random.Next(10, 50));

            this.Velocidad = random.Next(1, 3);

            this.X = (int)this._posicion.X;
            this.Y = (int)this._posicion.Y;
        }
    }
}
