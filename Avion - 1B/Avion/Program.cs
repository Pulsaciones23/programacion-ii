﻿using System;

namespace Avion
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Avion())
                game.Run();
        }
    }
}
