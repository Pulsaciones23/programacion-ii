﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Content;

namespace Avion
{
    public class Avion : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private Clases.Controlador _controlador;
        private ContentManager _context;
        const int _PANTALA_ALTO = 600;
        const int _PANTALLA_LARGO = 600;

        
        public Avion()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = _PANTALLA_LARGO;
            _graphics.PreferredBackBufferHeight = _PANTALLA_LARGO;
            _graphics.IsFullScreen = false;
            Window.Title = "Quick Math";
            Content.RootDirectory = "Content";
           
            IsMouseVisible = true;
           
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            this._spriteBatch = new SpriteBatch(GraphicsDevice);
            
            this._context = Content;
            _controlador = new Clases.Controlador(_PANTALA_ALTO, _PANTALLA_LARGO, _context, _spriteBatch);
            // TODO: use this.Content to load your game content here

        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            this._controlador.ACtualizar();
            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            _spriteBatch.Begin();
            this._controlador.Dibujar();
            _spriteBatch.End();
            base.Draw(gameTime);
        }
        protected override void UnloadContent()
        {
            base.UnloadContent();
        }
    }
}
